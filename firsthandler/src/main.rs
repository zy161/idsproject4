use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

// use serde_json::json;

#[derive(Deserialize)]
struct Request {
    sentence: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    length: i32,
}


async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // let body = event.query_string_parameters();
    let sentence = event.payload.sentence;
    
    let counts = sentence.len();

    let resp = Response{
        req_id: event.context.request_id,
        length: counts as i32,
    };
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
