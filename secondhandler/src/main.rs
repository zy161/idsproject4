use rand::{distributions::Alphanumeric, Rng};
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};


#[derive(Deserialize)]
struct Request {
    length: i32,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    rand_word: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Parse the character frequencies from the request body
    let word_len = event.payload.length;
    let mut rng = rand::thread_rng();
    let random_string: String = (0..word_len)
        .map(|_| rng.sample(Alphanumeric) as char)
        .collect();
    

    // Create a response
    let resp = Response{
        req_id: event.context.request_id,
        rand_word: random_string,
    };
    Ok(resp)
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
