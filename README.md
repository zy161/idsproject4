# IDS721 Project 4

## The Link to My Demo Video
- [**Check My Demo Video**](https://youtu.be/bk-TgnCMmJA)

## Create Lambda Functions
1. Use `cargo lambda new` command to create two new lambda functions
2. Add required dependencies for this project in `Cargo.toml`
3. Implement functions in the `main.rs` file, the first lambda function reads a sentence and get the length of it as output; the second lambda function will generate a random string with the same length from the first lambda's output
4. Use `cargo lambda watch` to test the functionality
5. Import `test_input.json` as data, test the functionality with `cargo lambda invoke --data-file test_input.json`
- `test_input.json` for the first lambda: 
```json
{"sentence": "hellohello"}
```
- <img src="img/firtest.png" style="width:600px;">

-- `test_input.json` for the second lambda: 
```json
{"length": 3}
```
- <img src="img/sectest.png" style="width:600px;">


## Create Step Function Workflow
1. In AWS Step Functions panel, create a new state machine
- <img src="img/definition.png" style="width:600px;">
2. Start an execution with test json: 
```json
"sentence": "hellohello"

```
The result of each step is shown below:
- <img src="img/first.png" style="width:600px;">
- <img src="img/second.png" style="width:600px;">

